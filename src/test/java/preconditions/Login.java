package preconditions;

import org.junit.Before;
import utils.ChromeSettings;

public class Login extends ChromeSettings {
    @Before
    public void login() {
        AdminLoginPage authorization = new AdminLoginPage(driver);
        authorization.login();
    }
}
