package preconditions;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("FieldCanBeLocal")
@Slf4j
public class AddCategoryPage {
    private WebDriver driver;
    private String category = "Men";

    //Locators
    private String nameFieldLocator = "name_1";
    private String saveButtonLocator = "category_form_submit_btn";
    private String categoryAddedLocator = "//div[@class='alert alert-success']/button";
    private String categoriesInTheTableLocator = "//tbody/tr//td[3]";
    private String lastCategoryInTheTableLocator = "//tbody/tr[last()]//td[3]";

    public AddCategoryPage(WebDriver driver) {
        this.driver = driver;
    }

    private void insertCategoryName() {
        driver.findElement(By.id(nameFieldLocator)).sendKeys(category);
    }

    private void clickSaveButton() {
        driver.findElement(By.id(saveButtonLocator)).click();
    }

    public boolean isCategoryAdded() {
        return driver.findElement(By.xpath(categoryAddedLocator)).isDisplayed();
    }

    private void waitForTheLastCategoryInTheTable(String category) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.textToBePresentInElement(driver.findElement(By.xpath(lastCategoryInTheTableLocator)), category));
    }

    public void addNewCategory() {
        insertCategoryName();
        clickSaveButton();
        waitForTheLastCategoryInTheTable(category);
    }

    public void getCategories() {
        List<String> categories = new ArrayList<>();
        List<WebElement> elements = driver.findElements(By.xpath(categoriesInTheTableLocator));
        for (WebElement e : elements) {
            categories.add(e.getText());
        }
        String title = "THE CATEGORIES IN ALPHABETICAL ORDER";
        System.out.println(title);
        log.info(title);

        List<String> sorted = categories.stream().sorted().collect(Collectors.toList());
        sorted.forEach(log::info);
        sorted.forEach(System.out::println);
    }
}