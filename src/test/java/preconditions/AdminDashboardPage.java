package preconditions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

@SuppressWarnings("FieldCanBeLocal")
public class AdminDashboardPage {
    private WebDriver driver;
    private WebElement element;

    //Locators
    private String catalogueLocator = "subtab-AdminCatalog";
    private String categoriesSubmenuLocator = "//li[@id='subtab-AdminCategories']/a";
//    private String categoriesSubmenuLocator = "//a[contains(text(), 'категории')]";

    public AdminDashboardPage(WebDriver driver) {
        this.driver = driver;
    }

    private void chooseCatalogueMenu() {
        Actions builder = new Actions(driver);
        element = driver.findElement(By.id(catalogueLocator));
        builder.moveToElement(element).build().perform();
    }

    private void clickCategoriesSubmenu() {
        element = driver.findElement(By.xpath(categoriesSubmenuLocator));
        element.click();
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.titleContains("категории"));
    }

    public CategoriesPage proceedToCategoriesSubmenu() {
        chooseCatalogueMenu();
        clickCategoriesSubmenu();
        return new CategoriesPage(driver);
    }
}
