package preconditions;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

@SuppressWarnings("FieldCanBeLocal")
class AdminLoginPage {
    private WebDriver driver;
    private WebElement element;

    //Locators
    private String emailLocator = "email";
    private String passwordLocator = "passwd";
    private String enterButtonLocator = "submitLogin";

    //Data to enter
    private String email = "webinar.test@gmail.com";
    private String password = "Xcg7299bnSmMuRLp9ITw";

    AdminLoginPage(WebDriver driver) {
        this.driver = driver;
    }

    private void enterEmail(String email) {
        this.email = email;
        element = driver.findElement(By.id(emailLocator));
        element.sendKeys(email);
    }

    private void enterPassword(String password) {
        this.password = password;
        element = driver.findElement(By.id(passwordLocator));
        element.sendKeys(password);
    }

    private void submitEnterButton() {
        element = driver.findElement(By.name(enterButtonLocator));
        element.submit();
    }

    void login() {
        enterEmail(email);
        enterPassword(password);
        submitEnterButton();
        new AdminDashboardPage(driver);
    }
}